<<<<<<< HEAD
# MiniCAD
CAD [1] (Computer-Aided Design or Computer-Aided Design and Drawing) is the use of technology to create designs (generally 2D) and replaces the manual drawing process of a piece with an automated one. One of the most popular such programs is AutoCAD. Such programs improve the designer's design quality and productivity.

ok know the actual program what i have done in it , i am gone explane every file below this line:
# Main.class
	This provides the entry point to the program, it accepts a file or directory. If a directory is passed in then all files in that directory will be used to create an image from the set instructions in the file.
# CAD.class
	Provides reference to set and get the pixels for the output image as well as getters for the width and height. Uses singleton pattern.
# Canvas.class
	A type of Shape which must be declared first in an instruction set. This contains the width and height of the output image as well as the background color
# Image.class
	Wrapper for the actual image, hides unnecessary methods and ensures pixels being set fall within the image bounds
# ShapeVisitor.class
	An interface to allow extensibility of the current program, e.g. printing to a physical printer, printing to screen etc.
# ShapeDrawVisitor.class
	This contains all the logic for drawing
# ShapeFactory.class
	Returns shapes based on the string array input. Returns shapes of the type below as well as Canvas. Uses Factory pattern.
		Circle
		Diamond
		Line
		Polygon
		Rectangle
		Square
		Triangle
# Shape
	Interface for all shape types. Uses visitor pattern.
=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
>>>>>>> origin/master
